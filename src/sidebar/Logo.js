import {Link} from "react-router-dom"
import styled from "styled-components"

const Logoo = styled.div`
    padding: 18px 20px;
    border-bottom: 2px solid white;

    & a {
        color: white;
        text-decoration: none;
        
        & span {
            color: #56D1A1;
        }
    }
`

const Logo = () => {
    return ( 
        <Logoo>
        <Link to="/">
            <span>MX</span>MARKET
        </Link>
        </Logoo>
     );
}
 
export default Logo;