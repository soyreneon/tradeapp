import { NavLink } from "react-router-dom"
import styled from "styled-components"

const Li = styled.li`
	a {
		padding: 3px 30px;
		text-transform: uppercase;
		display: block;
		color: #fff;
		text-decoration: none;
		font-size: 18px;

		&.active {
			background: #283141;
		}
		span {
			font-size: 19px;
			color: ${props => props.color};
			margin-right: 4px;
		}
	}
`
const NavItem = ({ color, name, ...rest }) => {
	return (
		<Li color={color}>
			<NavLink {...rest}>
				<span>&#9679;</span>
				{name}
			</NavLink>
		</Li>
	)
}

export default NavItem
