import styled from "styled-components"
import Close from "../assets/Close";
import { connect } from "react-redux"
import { openNavbar } from '../redux'

const CloseX = styled.div`
    display: none;
	@media only screen and (max-width: 576px) {
        display: block;
        text-align: right;
        background-color: rgba(0,0,0,0);
        color: white;
        > svg {
            margin: 5px;
            height: 20px;
            width: 20px;
        }
	}
`
const CloseButton = ({openNavbar}) => {
    return ( <CloseX onClick={openNavbar}>
        <Close />
    </CloseX> );
}

const mapDispatchToProps = dispatch => {
	return {
		openNavbar: () => dispatch(openNavbar()),
	}
}

export default connect(null, mapDispatchToProps)(CloseButton);