//import {Link} from "react-router-dom"
//import profile from '../assets/woman-profile.png';
import styled from 'styled-components';

const Profiler = styled.div`
    margin-top: 40px;
    border-bottom: 1px solid white;
    text-align: center;
    padding-bottom: 20px;

    & img{
        height: auto;
        width: 68px;
        display: block;
        margin: 0 auto;
    }
    & h2{
        margin-top: 13px;
        margin-bottom: 3px;
        font-size: 14px;
        font-weight: 600;
    }
    & h3{
        margin: 0px;
        font-size: 14px;
        font-weight: 500;
    }
`

const Profile = ({img, name, mail}) => {
	return (
		<Profiler>
				<img src={img} alt="profile" />
                <h2>{name}</h2>
                <h3>{mail}</h3>
		</Profiler>
	)
}

export default Profile
