import styled from "styled-components"
import NavItem from "./NavItem"
import Profile from "./Profile"
import Logo from "./Logo"
import profile from "../assets/woman-profile.png"
import { connect } from "react-redux"
import CloseButton from "./CloseButton"

const Aside = styled.aside`
	//width: 236px;
	overflow-y: auto;
	//transition: all 0.4s ease-out;
	/*
	&.closed {
		width: 0;
		transition: 0.4s ease-in;
		overflow-x: hidden;
	}*/
	& > div {
		background-color: #000000;
		/*position: fixed;
		left: 0;
		bottom: 0;
		transform: translateX(-236px);
		transition: transform .7s;
		*/
		transition: margin-left .7s;
		top: 0;
		width: 236px;
		height: 100%;
		//height: auto;
		margin-left: 0;
		&.closed{
			margin-left: 236px;
			//transform: translateX(0px);
		}
	}

	& nav {
		margin-top: 3em;
	}
	& ul {
		padding: 0;
	}

	@media only screen and (max-width: 576px) {
		& > div {
			position: fixed;
			left: 0;
			bottom: 0;
			transform: translateX(-236px);
			transition: transform .7s;
			&.closed{
				margin-left: 0px;
				transform: translateX(0px);
			}
		}
	}

	/*
	@media only screen and (min-width: 576px) {
	}
	@media only screen and (min-width: 768px) {
	}
	@media only screen and (min-width: 992px) {
	}
	@media only screen and (min-width: 1200px) {
	} 
	*/

`
const Sidebar = ({ navbaropen }) => {
	return (
		<Aside >
			<div className={navbaropen ? "open" : "closed"}>
			<CloseButton />
			<Logo />
			<Profile
				img={profile}
				name="Alejandra Langford"
				mail="a.langford@mxmarket.mx"
			/>
			<nav>
				<ul>
					<NavItem
						activeClassName="active"
						to="/user/workspace1"
						name="Workspace 1"
						color="#FFFFFF"
					/>
					<NavItem
						activeClassName="active"
						to="/user/workspace2"
						name="Workspace 2"
						color="#F56565"
					/>
					<NavItem
						activeClassName="active"
						to="/user/analisis"
						name="Analisis"
						color="#ECC94B"
					/>
					<NavItem
						activeClassName="active"
						to="/user/workspace4"
						name="Workspace 4"
						color="#48BB78"
					/>
					<NavItem
						activeClassName="active"
						to="/user/workspace5"
						name="Workspace 5"
						color="#38B2AC"
					/>
					<NavItem
						activeClassName="active"
						to="/user/workspace6"
						name="Workspace 6"
						color="#4299E1"
					/>
				</ul>
			</nav>
			</div>
		</Aside>
	)
}

const mapStateToProps = state => {
	return {
		navbaropen: state.navbar.navbaropen,
	}
}

export default connect(mapStateToProps)(Sidebar)
