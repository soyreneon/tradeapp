import React from "react"
import styled from "styled-components"
import moment from "moment"

const Meta = styled.div`
	display: flex;
	flex-direction: row;
	margin-bottom: 10px;
	margin-top: 7px;

	& div {
		flex-grow: 1;
		font-size: 12px;
		padding-left: 8px;
		border-right: 1px solid #666;
	}
	& div:last-child {
		border-right: none;
	}
`
const Metadata = ({ data }) => {
	const section = (title, value) => {
		return (
			<div>
				<h4>{title}</h4>
				<p>{value}</p>
			</div>
		)
	}
	return (
		<Meta>
			{section("SerieId", data.idSerie)}
			{section("Titulo", data.nombre)}
			{section(
				"Última fecha",
				moment(new Date(data.datos[data.datos.length - 1].fecha)).format(
					"DD/MM/YYYY"
				) /*fecha*/
			)}
			{section("Último valor", data.datos[data.datos.length - 1].dato)}
			{section("Unidad", data.unidad)}
			{section(
				"Cambio Porcentual",
				(100 *
					(data.datos[data.datos.length - 1].dato -
						data.datos[data.datos.length - 2].dato)) /
					data.datos[data.datos.length - 2].dato
			)}
			{section("Fecha Inicio", data.fechaInicio)}
			{section("Fecha Fin", data.fechaFin)}
			{section("Periodicidad", data.periodicidad)}
		</Meta>
	)
}

export default React.memo(Metadata)
