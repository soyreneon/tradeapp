import styled from "styled-components"
import DetailedData from "./DetailedData"
import { connect } from "react-redux"

const DetailsDiv = styled.div`
	flex-shrink: 1;
    width: auto;
	overflow-y: auto;
	height: auto;
`

const DetailsBox = ({ detailsopen }) => {
	return <DetailsDiv>{detailsopen && <DetailedData />}</DetailsDiv>
}

const mapStateToProps = state => {
	return {
		detailsopen: state.details.detailsopen,
	}
}

export default connect(mapStateToProps)(DetailsBox)
