import React from "react"
import styled from "styled-components"
import { connect } from "react-redux"
import Error500 from "./Error500"
import Loading from "./Loading"
import moment from "moment"
import Lines from "../../assets/Lines"

const Details = styled.div`
      width: 250px;
      & h2{
        background-color: #202837;
        font-size: 18px;
        font-weight: 500;
        padding: 9px 15px;
        & svg{
            margin-right: 5px;
            vertical-align: middle;
            height: 20px;
            width: 20px;
        }
      }
      & .table{
          font-size: 14px;
          & .row{
              display: flex;
              padding: 5px 10px;
              border-bottom: 1px solid #777;
              &:first-child{
                  background-color: #000;
                  border-bottom: 1px solid #FFF;
              }
              & h5 {
                  flex-grow: 1;
                  text-align: left;
                  font-weight: 400;
                  &:first-child{
                      width: 50%;
                  }
              }
          }
      }
`
const DetailedData = ({details}) => {
    return ( 
        <Details>
            {details.loading ? (
                <Loading />
            ) : details.error ? (
                <Error500>{details.error}</Error500>
            ) : (
                <div>
                    <h2><Lines />{details.nombre}</h2>
                    <div className="table">
                        <div className='row'>
                            <h5>Fecha</h5>
                            <h5>Valor</h5>
                        </div>
                        {details &&
                            details.details &&
                            details.details.map( (row,i) => (
                                <div key={i} className="row">
                                    <h5>{row.dato}</h5>
                                    <h5>{moment(new Date(row.fecha)).format('DD/MM/YYYY')}</h5>
                                </div>
                            ))
                        }
                    </div>
                </div>
            )
        }
        </Details>
     );
}

const mapStateToProps = state => {
	return {
        details: state.details, // loading details error
    }
}

export default React.memo(connect(mapStateToProps)(DetailedData));