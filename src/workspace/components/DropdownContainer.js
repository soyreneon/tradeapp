import React, { useState } from "react"
import styled from "styled-components"
import ButtonDown from "./ButtonDown"
import ChevronDown from '../../assets/ChevronDown'
import ChevronLeft from "../../assets/ChevronLeft"
import MoveUpDown from "../../assets/MoveUpDown"
import ChartIcon from "../../assets/ChartIcon"
import Lines from "../../assets/Lines"
import Metadata from "./Metadata"
import moment from "moment"
import { connect } from "react-redux"
import { openDetails } from '../../redux'

const Toggler = styled.div`
	border-radius: 4px;
	padding-top: 1px;
	background: #202837;
	color: white;
	margin-bottom: 15px;
	& svg {
		color: white;
		height: 19px;
		width: 19px;
	}
	& .content {
		max-height: 0;
		transition: all 0.2s ease-in;
		overflow: hidden;
		&.show {
			max-height: 100% !important;
			transition: all 0.6s ease-out;
			padding: 5px 15px;
		}
	}

	& header {
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		background-color: #000;
		padding: 2px 10px;
		margin-top: 5px;
		& button {
			background-color: rgba(0, 0, 0, 0);
			border: none;
		}
		& .right {
			text-align: right;
			flex-grow: 1;
		}
		& .left {
			flex-grow: 6;
			& h3 {
				font-size: 14px;
				font-weight: 600;
				display: inline-block;
				position: relative;
				margin-right: 25px;
				&::after {
					left: 107%;
					position: absolute;
					content: "|";
					color: #666;
				}
				&.thin{
					font-weight: 400;
				}
			}
			& svg {
				vertical-align: middle;
			}
		}
	}
`
const DropdownContainer = ({ children, data, openDetails }) => {
	const [mainclick, setMainClick] = useState(true)
	const [chartclick, setChartClick] = useState(true)
	const [detailclick, setDetailClick] = useState(false)

	const onHandleDetails = () => {
		setDetailClick(!detailclick)
		openDetails(data.nombre,data.datos)
		//detailclick ? openDetails() : openDetails(data.nombre,data.datos)
	}
	return (
		<Toggler>
			<header>
				<div className="left">
					<h3>{data.nombre}</h3>
					<h3 className='thin'>&#9679; {data.datos[data.datos.length-1].dato}</h3>
					<h3 className='thin'>{moment(new Date(data.datos[data.datos.length-1].fecha)).format('DD/MM/YYYY')}</h3>
					<ButtonDown
						active={chartclick}
						onClick={() => setChartClick(!chartclick)}>
						<ChartIcon />
					</ButtonDown>
					<ButtonDown
						active={detailclick}
						onClick={onHandleDetails}>
						<Lines />
					</ButtonDown>
				</div>
				<div className="right">
					<button>
						<MoveUpDown />
					</button>
					<button onClick={() => setMainClick(!mainclick)}>
						{mainclick ? (
							<ChevronDown />
						) : (
							<ChevronLeft />
						)}
					</button>
				</div>
			</header>
			<footer>
				<div className={`content ${mainclick ? "show" : ""}`}>
					<Metadata data={data}  />
					<div className={`content ${chartclick ? "show" : ""}`}>{children}</div>
				</div>
			</footer>
		</Toggler>
	)
}

const mapDispatchToProps = dispatch => {
	return {
		openDetails: (nombre, datos) => dispatch(openDetails(nombre, datos)),
	}
}

export default React.memo(connect(null, mapDispatchToProps)(DropdownContainer))