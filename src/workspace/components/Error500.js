import styled from "styled-components"

const Error = styled.div`
	text-align: center;
    margin: 40px 0;
`

function Error500({children}) {
	return (
		<Error>
			<h1>Error 500</h1>
			<p>{children}</p>
		</Error>
	)
}

export default Error500
