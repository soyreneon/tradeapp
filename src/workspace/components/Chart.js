import React, { useEffect, useRef } from "react"
import * as d3 from "d3"
import styled from "styled-components"

const ChartBox = styled.div`
	//background-color: #202837;
	width: 100%;
	height: 341px;
	border-top: 2px solid #FFF;

	& svg {
		height: 100%;
		width: 100%;
	}
`

const Chart = ({ plotData, idSerie=0 }) => {
	const d3Chart = useRef()
	const parseDate = d3.timeParse('%d/%m/%Y')
	
    useEffect(() => {
		const passData = () => {
			const plotDatad = plotData
			for (var j = 0; j < plotDatad.length; j++) {
				const newfecha = parseDate(plotDatad[j].fecha)
				//console.log(`${newfecha} - ${parseDate(newfecha)}`)
				Object.assign(plotDatad[j], {
					fecha: (newfecha),
				})
			}
			//console.log(plotDatad)
	
			const margin = { top: 20, right: 30, bottom: 30, left: 50 }
			const width = parseInt(d3.select(`#data-${idSerie}`).style("width")) - margin.left - margin.right
			const height = parseInt(d3.select(`#data-${idSerie}`).style("height")) - margin.top - margin.bottom
	
			// Set up chart
			const svg = d3.select(d3Chart.current)
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
	
			// x axis scale
			const x = d3.scaleTime()
				.domain(
					d3.extent(plotDatad, function (d) {
						return d.fecha
					})
				)
				.range([0, width])
	
			svg.append("g")
				.attr("transform", "translate(0," + height + ")")
				.call(d3.axisBottom(x))
	
			// Get the max value of counts
			const max = d3.max(plotDatad, function (d) {
				return d.dato
			})
	
			// y axis scale
			const y = d3.scaleLinear().domain([0, max]).range([height, 0])
	
			svg.append("g").call(d3.axisLeft(y))
	
			// Draw line
			svg.append("path")
				.datum(plotDatad)
				.attr("fill", "none")
				.attr("stroke", "white")
				.attr("stroke-width", 1)
				.attr(
					"d",
					d3
						.line()
						.x(function (d) {
							return x(d.fecha)
						})
						.y(function (d) {
							return y(d.dato)
						})
				)
			
			/*
			// Add title
			svg.append("text")
				.attr("x", width / 2 )
				.attr("y", margin.top / 5 - 10)
				.attr("text-anchor", "middle")
				.attr("font-size", "16px")
				.attr("fill", "white")
				.text("Grafica")
			*/
		}
		passData()
	}, [idSerie, plotData, parseDate])

    return <ChartBox id={`data-${idSerie}`} key={idSerie}>
        <svg ref={d3Chart}></svg>
        </ChartBox>
}

export default React.memo(Chart)
