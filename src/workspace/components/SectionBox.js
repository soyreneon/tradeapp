import React from "react"
import styled from "styled-components"

const Box = styled.div`
    background-color: #2D3748;
    height: auto;
    flex-grow: 5;
    overflow-y: auto;
    & div.child{
        padding-left: 10px;
    }
    & h2{
        background-color: #202837;
        margin-bottom: 25px;
        font-size: 18px;
        padding: 9px 15px;
    }
`

const SectionBox = ({title,children}) => {
    return ( 
        <Box>
            <h2>{title}</h2>
            <div className="child">
                {children}
            </div>
        </Box>
     );
}
 
export default React.memo(SectionBox);