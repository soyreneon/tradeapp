import React from 'react'
import styled, {css} from "styled-components"

const ToggleButton = styled.button`
    border: 1px solid #FFF !important;
	background-color: #000;
	margin-left: 10px;
	&:hover {background-color: #333;}

	${props =>
		props.active &&
		css`
			background-color: white !important;
			& svg{color: black !important;}
			&:hover{background-color: #DDD !important;}
		`}
`

const ButtonDown = ({ children, ...attributes }) => {
	console.log('button')
	return <ToggleButton {...attributes}>{children}</ToggleButton>
}

export default React.memo(ButtonDown)
