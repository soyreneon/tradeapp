import styled from "styled-components"
const PrincipalContainer = styled.div`
	flex: 1;
`
const Principal = ({ children }) => {
	return (
		<PrincipalContainer>
			{children}
		</PrincipalContainer>
	)
}

export default Principal
