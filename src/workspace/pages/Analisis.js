import React, { useEffect } from "react"
import DropdownContainer from "../components/DropdownContainer"
import { connect } from "react-redux"
import { fetchData } from "../../redux"
import Chart from "../components/Chart"
import Loading from "../components/Loading"
import SectionBox from "../components/SectionBox"
import Error500 from "../components/Error500"
import DetailsBox from "../components/DetailsBox"

const Analisis = ({ banxicoData, fetchData }) => {
	useEffect(() => {
		fetchData()
	}, [])
	return (
		<section>
			<h1>&#9679; Analisis</h1>
			<div className="inner">
				<SectionBox title="Gasto Federal">
					{banxicoData.loading ? (
						<Loading />
					) : banxicoData.error ? (
						<Error500>{banxicoData.error}</Error500>
					) : (
						<div>
							{banxicoData &&
								banxicoData.data &&
								banxicoData.data.map(row => (
									<DropdownContainer key={row.idSerie} data={row}>
										<Chart plotData={row.datos} id={row.idSerie} />
									</DropdownContainer>
								))}
						</div>
					)}
				</SectionBox>
				<DetailsBox />
			</div>
		</section>
	)
}

const mapStateToProps = state => {
	return {
		banxicoData: state.banxicoData,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		fetchData: () => dispatch(fetchData()),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Analisis)
