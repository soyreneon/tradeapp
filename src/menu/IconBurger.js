import styled from "styled-components"
import { connect } from "react-redux"
import { openNavbar } from '../redux'

const BurguerButton = styled.button`
	display: inline-block;
	vertical-align: middle;
	margin-right: 10px;
    background-color: rgba(0,0,0,0);
    border: none;
    cursor: pointer;

	& svg {
		height: 25px;
		width: 25px;
		color: white;
		display: inline-block;
	}
	& .burger {
	}
`
const IconBurger = ({openNavbar}) => {
	return (
		<BurguerButton className="burger" onClick={openNavbar}>
			<svg
				xmlns="http://www.w3.org/2000/svg"
				fill="none"
				viewBox="0 0 24 24"
				stroke="currentColor"
			>
				<path
					strokeLinecap="round"
					strokeLinejoin="round"
					strokeWidth="2"
					d="M4 6h16M4 12h16M4 18h16"
				/>
			</svg>
		</BurguerButton>
	)
}

const mapStateToProps = state => {
	return {
		navbaropen: state.navbar.navbaropen,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		openNavbar: () => dispatch(openNavbar()),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(IconBurger)
