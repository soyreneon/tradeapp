import styled from "styled-components"
import IconBurger from "./IconBurger"
import SearchBox from "./SearchBox"
import Gear from '../assets/Gear'
import Bell from '../assets/Bell'
import ChevronDown from "../assets/ChevronDown"

const HeaderMenu = styled.header`
	background-color: #0d0d0d;
	padding: 8px 15px;
	display: flex;
	flex-direction: row;
	justify-content: space-between;

	& .leftside {
		& svg {
			height: 20px;
			width: 20px;
			color: white;
			vertical-align: middle;
			margin-right: 7px;
		}
		& .others {
			display: inline-block;
			vertical-align: middle;
			margin-right: 10px;
		}
		& h2 {
			font-size: 13px;
			font-weight: 500;
			padding-left: 10px;
			display: inline-block;
			border-left: 1px solid white;

			& .chevron {
				display: inline-block;
				vertical-align: middle;
				margin-left: 5px;
			}
		}
	}

	@media only screen and (max-width: 768px) {
		.rightside{width: 100%}
		.leftside{ display: none; }
	}

	/*
	@media only screen and (min-width: 576px) {
	}
	@media only screen and (min-width: 768px) {
	}
	@media only screen and (min-width: 992px) {
	}
	@media only screen and (min-width: 1200px) {
		.colored {
		background-color: #00d !important;
		}
	} 
	*/

`
const Menu = () => {
	return (
		<HeaderMenu>
			<div className="rightside">
				<IconBurger />
				<SearchBox />
			</div>
			<div className="leftside">
				<div>
					<span className="others">
						<Gear />
						<Bell />
					</span>
					<h2>
						Alejandra
						<span className="chevron">
							<ChevronDown />
						</span>
					</h2>
				</div>
			</div>
		</HeaderMenu>
	)
}

export default Menu
