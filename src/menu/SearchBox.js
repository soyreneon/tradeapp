import styled from "styled-components"
import SearchIcon from "../assets/SearchIcon"

const Inputbox = styled.div`
	display: inline-block;
	border-radius: 15px;
	background-color: #2d3748;

	& span {
		padding: 8px 5px;
		font-size: 12px;

		& svg {
			height: 14px;
			width: 20px;
		}
	}
	& input {
		font-size: 12px;
		padding: 9px 5px;
		width: 386px;
		color: #fff;
		outline: none;
		border: none;
		background-color: rgba(0, 0, 0, 0);

		&:focus {
			outline: none;
		}
	}

	@media only screen and (max-width: 576px) {
		width: 80%;
		input {
			width: auto;
		}
	}
`
const SearchBox = () => {
	return (
		<Inputbox className="inputbox">
			<span>
				<SearchIcon />
			</span>
			<input
				type="text"
				placeholder="search securities, transactions, info or help"
			/>
		</Inputbox>
	)
}

export default SearchBox
