import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import GlobalStyle from "./general-styles/GeneralStyles"
import Login from "./login/Login"
// redux
import { Provider } from "react-redux"
import store from "./redux/store"
import UserView from "./login/UserView"

function App() {
	return (
		<div className="App">
			<Provider store={store}>
				<GlobalStyle />
				<Router>
					<Switch>
						<Route path="/user" component={UserView} />
						<Route path='/' exact component={Login} />
					</Switch>
				</Router>
			</Provider>
		</div>
	)
}

export default App
