import styled from "styled-components"
import { useState } from "react"
import Input from "./components/Input"
import Button from "./components/Button"

const LoginBox = styled.div`
	//padding: 10px 5px;
	display: flex;
	justify-content: center;
	align-items: center;
	height: 100vh;

	& > div {
		border: solid 1px #556;
		flex: 0;
        padding: 40px 0;
		flex-basis: 35%;
		height: auto;
	}
`
const Login = () => {
	const [user, setUser] = useState("")
	const [password, setPassword] = useState("")
	const onLogIn = e => {
        e.preventDefault()
        const obj = {user, password}
        console.log(obj)
        /*
        setID( oldid => ++oldid)
		const productobj = {
            id, name, price, number
        }
        addToBasket(productobj)*/
	}
	return (
		<LoginBox>
			<div>
				<form onSubmit={(e) => onLogIn(e)}>
					<Input
						setValue={setUser}
						id="user"
						type="text"
						name="user"
						type="text"
						placeholder="user"
						defaultValue=""
					/>
					<Input
						setValue={setPassword}
						id="user"
						type="text"
						name="user"
						type="text"
						placeholder="password"
						defaultValue=""
					/>
                    <Button>log in</Button>
				</form>
			</div>
		</LoginBox>
	)
}

export default Login
