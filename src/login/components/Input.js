import styled from "styled-components"

const InputBox = styled.div`
    display: block;
    margin: 15px auto;
    text-align: center;
    & label{
        color: white;
        text-transform: uppercase;
        display: block;
        font-size: 12px;
    }
    & input{
        margin: auto;
        font-size: 15px;
        padding: 3px 10px;
    }
`
const Input = ({
	setValue,
	name,
	id,
	placeholder,
	defaultValue = "",
	type = "",
}) => {
	return (
		<InputBox>
			<label htmlFor={id}>{placeholder}</label>
			<input
				id={id}
				placeholder={placeholder}
				defaultValue={defaultValue}
				onChange={e => setValue(e.target.value)}
				type={type}
				name={name}
			/>
		</InputBox>
	)
}

export default Input
