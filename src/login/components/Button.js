import styled from "styled-components"

const ButtonBox = styled.button`
	display: block;
	margin: 20px auto;
	background-color: #56d1a1;
	text-align: center;
	color: white;
	text-transform: uppercase;
	font-size: 14px;
    font-weight: 700;
    width: 216px;
    outline: none;
	padding: 6px 10px;
`
const Button = ({ children }) => {
	return <ButtonBox>{children}</ButtonBox>
}

export default Button
