import { Switch, Route } from "react-router-dom"
import Menu from "../menu/Menu"
import Sidebar from "../sidebar/Sidebar"
import Principal from "../workspace/components/Principal"
import Workspace1 from "../workspace/pages/Workspace1"
import Workspace2 from "../workspace/pages/Workspace2"
import Analisis from "../workspace/pages/Analisis"
import Workspace4 from "../workspace/pages/Workspace4"
import Workspace5 from "../workspace/pages/Workspace5"
import Workspace6 from "../workspace/pages/Workspace6"
import styled from "styled-components"

const UserBox = styled.div`
	display: flex;
	flex-direction: row;
	align-items: stretch;
	height: 100vh;
	/*new*/
	position: relative;
	left: -236px;
	width: calc(100% + 236px);
	@media only screen and (max-width: 576px) {
		position: initial;
		left: 0px;
		width: 100%;
	}
`
const UserView = () => {
	return (
		<UserBox>
			<Sidebar />
			<Principal>
				<Menu />
				<main>
					<Switch>
						<Route path="/user/workspace1" component={Workspace1} />
						<Route path="/user/workspace2" component={Workspace2} />
						<Route path="/user/analisis" component={Analisis} />
						<Route path="/user/workspace4" component={Workspace4} />
						<Route path="/user/workspace5" component={Workspace5} />
						<Route path="/user/workspace6" component={Workspace6} />
					</Switch>
				</main>
			</Principal>
		</UserBox>
	)
}

export default UserView
