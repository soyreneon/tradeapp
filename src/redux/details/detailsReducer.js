import { 
  OPEN_DETAILS,
} from './detailsTypes'

const initialState = {
  detailsopen: false,
  nombre: '',
  details: [],
}

const detailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_DETAILS: return {
      ...state,
      detailsopen: !state.detailsopen,
      nombre: action.name,
      details: action.payload,
    }

    default: return state
  }
}

export default detailsReducer