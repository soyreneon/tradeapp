import {
	OPEN_DETAILS,
} from './detailsTypes'

export const openDetails = (name='', details='') => {
  return {
    type: OPEN_DETAILS,
	name: name,
	payload: details,
	}
}
