import { endpoint, metadataarray } from "../../config/Config"
import {
	FETCH_DATA_REQUEST,
	FETCH_DATA_SUCCESS,
	FETCH_DATA_FAILURE,
} from "./banxicoTypes"

export const fetchData = () => {
	return dispatch => {
		dispatch(fetchDataRequest())
		Promise.all([
			fetch(endpoint).then(response => response.json()),
			fetch(metadataarray).then(resmeta => resmeta.json()),
		])
			.then(([response, resmeta]) => {
				const altnames = [
					{ idSerie: "SG46", nombre: "Gasto total" },
					{ idSerie: "SG47", nombre: "Gasto programable" },
					{ idSerie: "SG44", nombre: "Gasto corriente" },
					{ idSerie: "SG71", nombre: "Servicios personales" },
					{ idSerie: "SG73", nombre: "Materiales y suministros" },
					{ idSerie: "SG74", nombre: "Servicios generales y otros" },
					{ idSerie: "SG48", nombre: "Gasto de capital" },
					{ idSerie: "SG49", nombre: "Inversión física" },
					{ idSerie: "SG69", nombre: "Inversión financiera" },
					{ idSerie: "SG52", nombre: "Subsidios y transferencias" },
					{ idSerie: "SG75", nombre: "SyT corrientes" },
					{ idSerie: "SG76", nombre: "SyT capital" },
					{ idSerie: "SG53", nombre: "Gasto no programable" },
					{ idSerie: "SG42", nombre: "Participaciones" },
					{ idSerie: "SG45", nombre: "Adefas y otros" },
					{ idSerie: "SG72", nombre: "Costo financiero" },
				]

				const bmx = response.bmx.series
				const bmxmeta = resmeta.bmx.series
				for (var i = 0; i < bmx.length; i++) {
					for (var j = 0; j < bmx[i].datos.length; j++) {
						const newdato = bmx[i].datos[j].dato
						//console.log(`${newdato} - ${newdato.replace(/,/gi, '')} - ${parseFloat(newdato.replace(/,/gi, ''))}`)
						Object.assign(bmx[i].datos[j], {
							dato: parseFloat(newdato.replace(/,/gi, "")),
						})
					}
				}
				let merged = []
				for (let i = 0; i < bmx.length; i++) {
					merged.push({
						...bmx[i],
						...altnames.find(itmInner => itmInner.idSerie === bmx[i].idSerie),
						...bmxmeta.find(itmInner => itmInner.idSerie === bmx[i].idSerie),
					})
				}
				//console.log(merged)
				//console.log(response)
				//console.log(resmeta)
				dispatch(fetchDataSuccess(merged))
			})
			.catch(error => {
				dispatch(fetchDataFailure(error.message))
			})
	}
}

export const fetchDataRequest = () => {
	return {
		type: FETCH_DATA_REQUEST,
	}
}

export const fetchDataSuccess = data => {
	return {
		type: FETCH_DATA_SUCCESS,
		payload: data,
	}
}

export const fetchDataFailure = error => {
	return {
		type: FETCH_DATA_FAILURE,
		payload: error,
	}
}
