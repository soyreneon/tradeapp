import { combineReducers } from 'redux'
import navbarReducer from './navbar/navbarReducer'
import banxicoReducer from './banxico/banxicoReducer'
import detailsReducer from './details/detailsReducer'

const rootReducer = combineReducers({
  navbar: navbarReducer,
  details: detailsReducer,
  banxicoData: banxicoReducer
})

export default rootReducer