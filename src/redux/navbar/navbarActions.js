import { OPEN_NAVBAR } from './navbarTypes'

export const openNavbar = () => {
  return {
    type: OPEN_NAVBAR
  }
}