import { OPEN_NAVBAR } from './navbarTypes'

const initialState = {
  navbaropen: true
}

const navbarReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_NAVBAR: return {
      ...state,
      navbaropen: !state.navbaropen
    }

    default: return state
  }
}

export default navbarReducer