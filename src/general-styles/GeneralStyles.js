import { createGlobalStyle } from "styled-components"

const GlobalStyle = createGlobalStyle`
    body {
      background-color: #1A202C;
      margin:0;
      font-family: 'Open Sans', sans-serif;
    }
    h1,h2,h3,h4,h5,p,span{ color: white; margin: 0px;}
    ::-webkit-scrollbar {
      width: 6px;
      border-left: 4px solid #56D1A1;
    }
  	::-webkit-scrollbar-thumb {
      background-color: #DDD;
    }
  main > section{
      margin-left: 25px;
      margin-top: 10px;
      height: 100%;
    }
  main > section > div.inner{
      display: flex;
      flex-direction: row;
      align-items: stretch;
      height: 83vh;
    }
  main > section > h1{
      margin-bottom: 10px;
      font-size: 16px;
      font-weight: 500;
      text-transform: uppercase;
      color: #ECC94B;
    }
    /*@media only screen and (max-width: 576px) {
      .App{
      }
    }*/
  `
export default GlobalStyle
