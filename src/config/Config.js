export const token = '72a5ecb455c7892ad7804d5de61500880c3dbe16a5ffb2cd162b15b7d0c8f294'
export const endpoint = `https://www.banxico.org.mx/SieAPIRest/service/v1/series/SG46,SG47,SG44,SG71,SG73,SG74,SG48,SG49,SG69,SG52,SG75,SG76,SG53,SG42,SG45,SG72/datos/2019-01-01/2021-02-01?token=${token}`
export const metadataarray = `https://www.banxico.org.mx/SieAPIRest/service/v1/series/SG46,SG47,SG44,SG71,SG73,SG74,SG48,SG49,SG69,SG52,SG75,SG76,SG53,SG42,SG45,SG72?token=${token}`
export const metadata = `https://www.banxico.org.mx/SieAPIRest/service/v1/series/`
